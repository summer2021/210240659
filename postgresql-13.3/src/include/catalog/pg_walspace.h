/*-------------------------------------------------------------------------
 *
 * pg_walspace.h
 *	  definition of the "walspace" system catalog (pg_walspace)
 *
 *
 * Portions Copyright (c) 1996-2020, PostgreSQL Global Development Group
 * Portions Copyright (c) 1994, Regents of the University of California
 *
 * src/include/catalog/pg_walspace.h
 *
 * NOTES
 *	  The Catalog.pm module reads this file and derives schema
 *	  information.
 *
 *-------------------------------------------------------------------------
 */

#ifndef PG_WALSPACE_H
#define PG_WALSPACE_H

#include "catalog/genbki.h"
#include "catalog/pg_walspace_d.h"

/* ----------------
 *		pg_walspace definition.  cpp turns this into
 *		typedef struct FormData_pg_walspace
 * ----------------
 */
CATALOG(pg_walspace,9704,WalSpaceRelationId) BKI_SHARED_RELATION
{
    NameData walspacename;                   /*walspace member's name*/
    NameData status BKI_DEFAULT(UNUSED);     /*walspace member's status*/
    NameData addres;                             /*wal output addres*/
} FormData_pg_walspace;

/* ----------------
 *		Form_pg_walspace corresponds to a pointer to a tuple with
 *		the format of pg_walspace relation.
 * ----------------
 */
typedef FormData_pg_walspace *Form_pg_walspace;
#endif // PG_WALSPACE_H
