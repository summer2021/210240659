当前的postgresql-13.3为更新后的版本
    1.使用时像平常正常使用pg即可，当pg执行完initdb命令后可直接在数据库中查询pg_walspace系统表
    2.在运行过程中，如果需要切换路径，需要在想要切换的路径先创建相应的文件夹，然后将该文件夹的路径插入pg_walspace中，需要注意的是，插入时新路径的状态为UNUSED。
    3.插入成功后，调用switch指令，进行路径状态的切换。切换后可查看pg_walspace系统表中的成员情况
    4.调用switch切换状态成功后，手动执行checkpoint执行请求创建检查点，执行完成后可以看到pg_walspace系统表中状态为ACTIVE的成员状态变为UNACTIVE，且此时新路径下多出了新的Wal段文件。
